class calculator:
    def __init__(self):
        self.sumbol = {"(" : 0, 
                       "+" : 1, 
                       "-" : 1, 
                       "*" : 2, 
                       "/" : 2, 
                       "^" : 3, 
                       ")" : 0}

    def calculator(self, value: str)->str:
        arr = self.parser_expression(value)
        arr = self.encode_RPN(arr)
        ans = value + " = " + str(self.RPN(arr))
        return ans

    def encode_RPN(self, arr: list):
        print(arr) 
        open_bracket = 0
        stec_sumbol = []
        ans = []
        for i in arr:
            if not ( i in self.sumbol ) :
                if ( '.' in i ) :
                    ans.append(float(i))
                else :
                    ans.append(int(i))
            else:
                if ( i == "(" ) :
                    stec_sumbol.append(i)
                    open_bracket += 1

                elif ( i == ")") :
                    if (open_bracket == 0):
                        return None
                    while ( stec_sumbol[-1] != "(" ) :
                        ans.append(stec_sumbol[-1])
                        stec_sumbol.pop()
                    stec_sumbol.pop()
                    open_bracket -= 1
                    
                else :
                    while ( len(stec_sumbol) != 0 and self.sumbol[stec_sumbol[-1]] >= self.sumbol[i] ) :
                        ans.append(stec_sumbol[-1])
                        stec_sumbol.pop()
                    stec_sumbol.append(i)

        ans += stec_sumbol[::-1]
        return ans if open_bracket == 0 else None

    def RPN(self, arr: list):
        stec_rpn = []
        for i in arr:
            if not ( i in self.sumbol ):
                stec_rpn.append(i)
            else:
                value = stec_rpn[-1]
                stec_rpn.pop()
                match i:
                    case "-":
                        stec_rpn[-1] -= value
                    case "+":
                        stec_rpn[-1] += value  
                    case "*":
                        stec_rpn[-1] *= value
                    case "/":
                        stec_rpn[-1] /= value 
                    case "^":
                        stec_rpn[-1] **= value 

        return stec_rpn[-1] if len(stec_rpn) == 1 else None

    def parser_expression(self, value: str):
        ans = []
        sign = True
        value_number = ""
        number = False
        for i in value:
            if i in self.sumbol:
                if number :
                    if not(value_number.isspace()):
                        ans.append(value_number)
                    value_number = ""
                    number = False
                if sign:
                    match i:
                        case '(', ')':
                            ans.append(i)
                        case '+':
                            ans.append("1")
                            ans.append("*")
                        case '-':
                            ans.append("-1")
                            ans.append("*")
                        case _ :
                            return None
                else:
                    ans.append(i)

                sign = True
            
            else:
                sign = False
                number = True
                value_number += i
        if number:
            ans.append(value_number)

        return ans
