from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import InlineQuery, InputTextMessageContent,\
                          InlineQueryResultArticle
from time import time, asctime, localtime
from calculator import calculator

class botCalculator:
    def __init__(self, token: str):
        self.description =  """
A calculator bot is a bot that is designed to perform mathematical operations. It can perform addition, subtraction, multiplication, and division, and it can also use parentheses to group operations. The calculator bot can also raise numbers to a power, which allows you to perform more complex calculations. This bot is very handy for anyone who needs fast and accurate math problem solving."""
        try:
            bot = Bot(token = token)
            dp = Dispatcher(bot)
        except Exception as err:
            print("Bot did not start due to:", err)
            exit()
        self.calculator = calculator()

        self.dp = self.create_message(bot, dp)
        executor.start_polling(dp)
        
    def create_message(self, bot: Bot, dp: Dispatcher)->Dispatcher:
        @dp.message_handler(commands=["start"])
        async def process_start_command(message: types.Message)->None:
            botCalculator.time_message(message)
            await bot.send_message(message.from_user.id, self.description)

        @dp.message_handler()
        async def process_message(message: types.Message)->None:
            botCalculator.time_message(message)
            ans = self.calculator.calculator(message.text)
            await bot.send_message(message.from_user.id, ans)

        @dp.inline_handler()
        async def inline_message(inline_query: InlineQuery)->None:
            if not(len(inline_query.query)):
                return None
            ans = self.calculator.calculator(inline_query.query)
            botCalculator.time_inline(inline_query)
            results =  [ InlineQueryResultArticle(
                id="1",
                title= ans,
                input_message_content=InputTextMessageContent(ans)
                ) ]

            await bot.answer_inline_query(inline_query.id, results=results, 
                                          cache_time=1)
    @staticmethod
    def time_message(message: types.Message)->None:
        value_time = asctime(localtime(time()))
        user_name = message.from_user.first_name
        user_id = message.from_user.id
        print("Classic mode:", value_time, user_name, user_id)
        return None

    @staticmethod
    def time_inline(inline: InlineQuery)->None:
        value_time = asctime(localtime(time()))
        user_name = inline.from_user.first_name
        user_id = inline.id
        print("Inline mode: ", value_time, user_name, user_id)
        return None
